import { Component } from '@angular/core';
import {medService} from "./med.service";
import {HttpErrorResponse} from "@angular/common/http"  
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/css/style.css']
})
export class AppComponent {
  //Model Variable 
  leadList:Array<any> = [];

  constructor(private _medservice:medService){}
  //making object ready for input fields
  leadObject:any = {
    fdate  :'',
    todate: '',
    email : '',
    phone_number: '',
    leadupload:'',
    agent:'',
    supervisor:'',
    ftask :'',
    status:'',
    timerange: '',
  }

  //clearing the input fields
  cleardata(){
    this.leadObject={};
  }

  private data:any;

  //search lead function implimentation
  searchLead()
  {
              /* Req Header
                let reqHeader:any= {
                    agent:this.leadObject.agent,
                    email: this.leadObject.email,
                    fromDate: this.leadObject.fdate,
                    mobile: this.leadObject.phone_number,
                    rowCount: 20,
                    rowIndex: 0,
                    selTask: this.leadObject.ftask,
                    supervisor: this.leadObject.supervisor,
                    toDate: this.leadObject.todate,
                    upload: this.leadObject.leadupload
                }  
                */

        //Rest Service Calling
        //return this._medservice.searchInsert(this.objdata).subscribe(this._successCallBack,this._errorCallBack)   
      
       
        
  }
  
    //_successCallBack
  private _successCallBack=(res):any=>{
        this.leadList=res;
       
  }

  //error handling machanisim

  private _errorCallBack=(err:HttpErrorResponse):any=>{
      if(err.error instanceof Error)
      {
        console.log("Client Side Error!");
      }
      else
      {
         console.log("Server Side Error!")
      }
  }

}
