import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ReactiveFormsModule,FormsModule} from "@angular/forms";
//date picker module
import {BsDatepickerModule} from "ngx-bootstrap/datepicker"
//countrys module
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
//med medService
import {medService} from "./med.service";

import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,BsDatepickerModule.forRoot(),FormsModule,InternationalPhoneNumberModule,ReactiveFormsModule,HttpClientModule
  ],
  providers: [medService],
  bootstrap: [AppComponent]
})
export class AppModule { }
